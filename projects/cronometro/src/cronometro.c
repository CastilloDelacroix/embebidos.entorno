/* Copyright 2017, Esteban Volentini - Facet UNT, Fi UNER
 * Copyright 2014, 2015 Mariano Cerdeiro
 * Copyright 2014, Pablo Ridolfi
 * Copyright 2014, Juan Cecconi
 * Copyright 2014, Gustavo Muro
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/** @file cronometro.c
 **
 ** @brief Ejemplo de un cronometro deportivo
 **
 ** Ejemplo de un led parpadeando utilizando la capa de abstraccion de 
 ** hardware y sin sistemas operativos.
 ** 
 ** | RV | YYYY.MM.DD | Autor       | Descripción de los cambios              |
 ** |----|------------|-------------|-----------------------------------------|
 ** |  2 | 2017.10.16 | evolentini  | Correción en el formato del archivo     |
 ** |  1 | 2017.09.21 | evolentini  | Version inicial del archivo             |
 ** 
 ** @defgroup ejemplos Proyectos de ejemplo
 ** @brief Proyectos de ejemplo de la Especialización en Sistemas Embebidos
 ** @{ 
 */

/* === Inclusiones de cabeceras ============================================ */
#include "FreeRTOS.h"
#include "task.h"
#include "unt.h"
#include "soc.h"

/* === Definicion y Macros ================================================= */

/* === Declaraciones de tipos de datos internos ============================ */

/* === Declaraciones de funciones internas ================================= */

/** @brief Función que implementa una tarea de baliza
 ** 
 ** @parameter[in] parametros Puntero a una estructura que contiene el led
 **                           y la demora entre encendido y apagado.
 */ 
void Refresco(void * parametros);

void Temporizador(void * parametros);

void Suspender(void * parametros);

/* === Definiciones de variables internas ================================== */
uint8_t pantalla[4] = {1,2,3,4};
uint8_t pantallaAnterior[4] = {0} ;

bool contando = TRUE;
bool suspendida = FALSE;

TaskHandle_t TareaDescartar;

/* === Definiciones de variables externas ================================== */

/* === Definiciones de funciones internas ================================== */

void Refresco(void * parametros) {
   uint8_t actual = 0;
   while(1) {
      Escribir_Digito(pantalla[actual], actual);
      vTaskDelay(5 / portTICK_PERIOD_MS);
      actual = (actual + 1) % 4;
   }
}
void Led(void * parametros) {
   bool prendido = TRUE;
   while (1) {
      vTaskDelay(100 / portTICK_PERIOD_MS);

      if (contando) {
         if (prendido) {
            Led_RGB(0, 0, 100);
         } else {
            Led_RGB(0, 0, 0);
         }
         prendido = !prendido;
      } else {
         Led_RGB(0, 100, 0);
         prendido = TRUE;
      }
   }   
}

void Temporizador(void * parametros) {
   TickType_t UltimoValor;

   UltimoValor = xTaskGetTickCount();
   while(1) {
      vTaskDelayUntil(&UltimoValor, pdMS_TO_TICKS(1000));
      /* Incremento de la cuenta */
      if (contando) {
         pantalla[3]++;
      }
      if (pantalla[3] == 10) {
         pantalla[3] = 0;
         pantalla[2]++;
      }
      if (pantalla[2] == 6) {
         pantalla[2] = 0;
         pantalla[1]++;
      }
      if (pantalla[1] == 10) {
         pantalla[1] = 0;
         pantalla[0]++;
      }
      if (pantalla[0] == 6) {
         pantalla[0] = 0;
      }
   }
}

void Teclado(void * parametros) {
   uint8_t tecla;

   while(1) {
      do {
         vTaskDelay(100 / portTICK_PERIOD_MS);
         tecla = Leer_Teclas();
      } while (tecla == NO_KEY);

      if (tecla == TECLA_SI ) {
         contando = !contando;
      }
      if ((tecla == TECLA_NO) && (!contando)) {
         pantalla[0] = 0;
         pantalla[1] = 0;
         pantalla[2] = 0;
         pantalla[3] = 0;
      }

      //Funcionalidad agregada para la tecla F1 - Congelar la cuenta
      if (tecla == TECLA_F1)  {
         //pregunta si estoy contando
         if (contando) {
         //pregunto si la cuenta esta suspendida
          if (suspendida) {
            suspendida = FALSE;
            //Descarto la tarea, y me voy a refrescar la cuenta actual
            vTaskResume(TareaDescartar);
           }
          else {
             //Gardo el valor de la cuenta suspendida
            pantallaAnterior[0] = pantalla[0];
            pantallaAnterior[1] = pantalla[1];
            pantallaAnterior[2] = pantalla[2];
            pantallaAnterior[3] = pantalla[3];
            vTaskSuspend(TareaDescartar);
            suspendida = TRUE;
              
            }
         }
     }
      do {
         vTaskDelay(100 / portTICK_PERIOD_MS);
         tecla = Leer_Teclas();
      } while (tecla != NO_KEY);
   }
}

void Suspender(void * parametros) {
   uint8_t actual = 0;

   while(1) {
      if (suspendida) {
         Escribir_Digito(pantallaAnterior[actual], actual);
         vTaskDelay(5 / portTICK_PERIOD_MS);
      }
      
      actual = (actual + 1) % 4; 
    }
}

  

/* === Definiciones de funciones externas ================================== */

/** @brief Función principal del programa
 **
 ** @returns 0 La función nunca debería termina
 **
 ** @remarks En un sistema embebido la función main() nunca debe terminar.
 **          El valor de retorno 0 es para evitar un error en el compilador.
 */
int main(void) {
   /* Inicializaciones y configuraciones de dispositivos */
   SisTick_Init();
   Init_PonchoUNT();

   /* Creación de las tareas */
   xTaskCreate(Suspender, "Suspender", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
   xTaskCreate(Refresco, "Refresco", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 3, &TareaDescartar);
   xTaskCreate(Temporizador, "Temporizador", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 5, NULL);
   xTaskCreate(Led, "Led", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 2, NULL);
   xTaskCreate(Teclado, "Teclado", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 4, NULL);

   /* Arranque del sistema operativo */
   vTaskStartScheduler();
   
   /* vTaskStartScheduler solo retorna si se detiene el sistema operativo */
   while(1);

   /* El valor de retorno es solo para evitar errores en el compilador*/
   return 0;
}
/* === Ciere de documentacion ============================================== */
/** @} Final de la definición del modulo para doxygen */
